using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnLayer : MonoBehaviour
{
    Vector2 startPos;

    // Start is called before the first frame update
    private void Start()
    {
        startPos = transform.position;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Deadzone"))
        {
            Die();
        }
    }

    void Die()
    {
        StartCoroutine(Respawn(1f));
    }

    public IEnumerator Respawn(float duration)
    {
        yield return new WaitForSeconds(duration);
        transform.position = startPos;
        gameObject.GetComponent<Animator>().SetBool("isDead",false);
    }
}

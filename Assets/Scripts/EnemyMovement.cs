﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    public float moveSpeed; 
    public float moveDistance; 
    public Transform leftPoint; 
    public Transform rightPoint;
    public Animator anim;
    public int dameEnemy = 10;
    public int maxHP = 100;
    int currentHP = 0;
    public float delayAttack = 0;
    public float attackRange = 0.5f;
    public Transform attackPoint;
    public Rigidbody2D ridEnemy;

    private bool a = false;
    private bool movingRight = true;

   
    private void Start()
    {
        currentHP = maxHP;
    }
    public void TakeDamage(int dame)
    {
        currentHP -= dame;
        anim.SetTrigger("Hurt");

        if(currentHP <= 0)
        {
            Die();
            Debug.Log($"DIE!!!!");
        }
    }
    public void Die()
    {
        moveSpeed = 0;
        anim.SetBool("IsDead", true);
        //wait 1s before Destroy
        StartCoroutine(DestroyAfterDelay(1f));
    }
    private IEnumerator DestroyAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        Destroy(gameObject);
    }
    void Update()

    {
        delayAttack -= Time.deltaTime;
        if (!a)
        {
            if (movingRight)
            {
                // Di chuyển quái vật sang phải đến vị trí điểm bên phải
                transform.position = Vector3.MoveTowards(transform.position, rightPoint.position, moveSpeed * Time.deltaTime);
                transform.localScale = rightPoint.localScale;
                // Nếu quái vật đã đi đến vị trí điểm bên phải, quay đầu lại và di chuyển sang trái
                if (transform.position.x >= rightPoint.position.x)
                {
                    movingRight = false;
                }
            }
            else
            {
                // Di chuyển quái vật sang trái đến vị trí điểm bên trái
                transform.position = Vector3.MoveTowards(transform.position, leftPoint.position, moveSpeed * Time.deltaTime);
                transform.localScale = leftPoint.localScale;
                // Nếu quái vật đã đi đến vị trí điểm bên trái, quay đầu lại và di chuyển sang phải
                if (transform.position.x <= leftPoint.position.x)
                {
                    movingRight = true;
                }
            }
        }
    }
    //monster's zone
    /*private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            transform.position = Vector3.MoveTowards(transform.position, collision.transform.position, moveSpeed * Time.deltaTime);
            a = true;
            Flip(collision.transform);
            Attack(collision.GetComponent<PlayerControllerTest>());

        }


    }*/
    //out zone
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            a = false;
        }
    }
    private void Flip(Transform TransformPlayer)
    {

        if (transform.position.x > TransformPlayer.position.x)
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
        else
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
    }
    private void OnTriggerStay(Collider collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            a = true;
            Vector2 target = new Vector2(collision.transform.position.x, ridEnemy.position.y);
            if (Vector2.Distance(collision.transform.position, ridEnemy.position) <= attackRange)
            {
                if (delayAttack <= 0)
                {
                    delayAttack = 2;
                    Attack(collision.gameObject.GetComponent<PlayerControllerTest>());
                }
            }
            else
            {
                transform.position = Vector3.MoveTowards(transform.position, collision.transform.position, moveSpeed * Time.deltaTime);
            }
            Flip(collision.transform);
        }
    }
    private void Attack(PlayerControllerTest player)
    {
        anim.SetTrigger("Attack");
        //StartCoroutine(player.TakeDame(dameEnemy));
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerTest : MonoBehaviour
{
    public enum State { Run_Left = 0, Run_Right = 1, Jump = 2, Idle = 3, Fall = 4, Attack = 5 }
    public enum MoveState { Move_Left = State.Run_Left, Move_Right = State.Run_Right }
    private static string NAMEANIM_RUN = "Run";
    private static string NAMEANIM_IDLE = "Idle";
    private static string NAMEANIM_JUMP = "Jump";
    private static string NAMEANIM_FALL = "Fall";
    //private static string NAMEANIM_ATTACK = "Attack";

    public float speed;
    public float jumpForce;
    public float moveInput;
    public int attackDame = 10;
    public Transform groundCheck;
    public LayerMask groundLayer;
    public Animator anim;
    public Transform attackPoint;
    public float attackRange = 0.5f;
    public LayerMask enemyLayers;
    public float attackRate = 2f;
    float nextAttackTime = 0f;
    public int currentHP = 100;

    private Rigidbody2D rigidbody2D;
    private bool isGrounded;
    private bool isJumping = false;
    private bool isFalling = false;
    //private bool isAttacking = false;

    private bool CheckGround = true;
    void Start()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }
    void Update()
    {
        InputSys();
        Look();
        isGrounded = IsGround();

        rigidbody2D.velocity = new Vector2(moveInput * speed, rigidbody2D.velocity.y);
        if (isGrounded)
        {
            if (isJumping)
            {
                // Nếu player đã kết thúc việc nhảy, đặt isJumping về false
                rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, 0);
                isJumping = false;
            }
            if (moveInput > 0)
            {
                Move(MoveState.Move_Right);
            }
            else if (moveInput < 0)
            {
                Move(MoveState.Move_Left);

            }
            else if (Input.GetKeyDown(KeyCode.J) && Time.time >= nextAttackTime)
            {
                // Bắt đầu thực hiện animation attack
                Attack();
                nextAttackTime = Time.time + 1f / attackRate;
            }
            else
            {
                if (!this.anim.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
                {
                    Idle();
                }
            }

            if (Input.GetKeyDown(KeyCode.Space) && !isJumping)
            {
                // Debug.Log($"   CheckGround = { CheckGround} isJumping {isJumping}");
                Jump();
                CheckGround = false;
                Invoke(nameof(ActiveCheckGround), 0.2f);
            }

        }
    }

    private bool IsGround()
    {
        // Kiểm tra player có đang ở trên mặt đất hay không
        bool isGrounded = Physics2D.Raycast(transform.position, Vector2.down, 1.2f, groundLayer) && CheckGround;

        return isGrounded;
    }
    private void Move(MoveState moveState)
    {
        switch (moveState)
        {
            case MoveState.Move_Left:
                anim.Play(NAMEANIM_RUN);
                break;
            case MoveState.Move_Right:
                anim.Play(NAMEANIM_RUN);
                break;
        }
    }
    private void Look()
    {
        if (moveInput > 0)
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
        else if (moveInput < 0)
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
    }
    private void ActiveCheckGround()
    {
        CheckGround = true;
    }
    private void Jump()
    {
        rigidbody2D.AddForce(Vector2.up * jumpForce);
        isJumping = true;
        anim.Play(NAMEANIM_JUMP);
    }
    private void Fall()
    {
        if (GetComponent<Rigidbody2D>().velocity.y < 0)
        {
            anim.SetBool("Fall", true);
        }
    }
    private void Idle()
    {
        anim.Play(NAMEANIM_IDLE);
    }
    private void InputSys()
    {
        moveInput = Input.GetAxisRaw("Horizontal");
    }
    void Attack()
    {
        anim.SetTrigger("Attack");
        Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, enemyLayers);

        foreach (var item in hitEnemies)
        {
            Debug.Log("hit!!!" + item.gameObject.name);
            item.GetComponent<EnemyMovement>().TakeDamage(attackDame);
        }
    }
    void OnDrawGizmosSelected()
    {
        if (attackPoint == null)
        {
            return;
        }
        Gizmos.DrawWireSphere(attackPoint.position, attackRange);
    }
    public void TakeDame(int dame)
    {
        currentHP -= dame;
        anim.SetTrigger("Hurt");

        if (currentHP <= 0)
        {
            Die();
            Debug.Log($"DIE!!!!");
        }
    }
    private void Die()
    {
        anim.SetBool("isDead",true);
        StartCoroutine(gameObject.GetComponent<SpawnLayer>().Respawn(2f));
    }


}
